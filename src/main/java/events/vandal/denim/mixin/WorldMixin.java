package events.vandal.denim.mixin;

import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.server.DenimServer;
import net.minecraft.registry.RegistryKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldGenerationProgressListener;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.random.RandomSequencesState;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionOptions;
import net.minecraft.world.dimension.DimensionTypes;
import net.minecraft.world.level.ServerWorldProperties;
import net.minecraft.world.level.storage.LevelStorage;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.BooleanSupplier;

@Mixin(ServerWorld.class)
public abstract class WorldMixin {
    @Shadow @Nullable public abstract MinecraftServer getServer();

    @Shadow @Final private ServerWorldProperties worldProperties;

    @Inject(at = @At("TAIL"), method = "<init>")
    public void getServerAndWorld(MinecraftServer server, Executor workerExecutor, LevelStorage.Session session, ServerWorldProperties properties, RegistryKey worldKey, DimensionOptions dimensionOptions, WorldGenerationProgressListener worldGenerationProgressListener, boolean debugWorld, long seed, List spawners, boolean shouldTickTime, RandomSequencesState randomSequencesState, CallbackInfo ci) {
        // For some reason they're not equal unless I do this
        if (Objects.equals(worldKey.getValue().toString(), DimensionTypes.THE_END_ID.toString())) {
            if (this.getServer() != null)
                DenimServer.Companion.setServer(this.getServer());

            // Since logically it should exist, add it for the love of god
            /*World world = Objects.requireNonNull(this.getServer()).getWorld(worldKey);
            assert world != null;
            DenimServer.Companion.setEndWorld(world);*/

            // Never mind I'm fucking stupid
            DenimServer.Companion.setEndWorld((World) (Object) this);
        }
    }

    @Inject(at = @At("HEAD"), method = "tick")
    public void tick(BooleanSupplier shouldKeepTicking, CallbackInfo ci) {
        if (((World)(Object) this).equals(DenimServer.Companion.getEndWorld())) {
            FakeDragonBattleManager.INSTANCE.tick();
            DragonBattleManager.INSTANCE.tick();
        }

        DenimServer.Companion.getServer().getPlayerManager().getPlayerList().stream().filter((player) -> player.hasNoGravity()).forEach((player) -> {
            player.setNoGravity(false);
        });
    }
}
