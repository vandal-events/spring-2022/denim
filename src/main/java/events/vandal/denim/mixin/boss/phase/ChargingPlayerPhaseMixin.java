package events.vandal.denim.mixin.boss.phase;

import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.phase.HomingSweepManager;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.phase.AbstractPhase;
import net.minecraft.entity.boss.dragon.phase.ChargingPlayerPhase;
import net.minecraft.entity.boss.dragon.phase.PhaseType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ChargingPlayerPhase.class)
public abstract class ChargingPlayerPhaseMixin extends AbstractPhase {
    public ChargingPlayerPhaseMixin(EnderDragonEntity dragon) {
        super(dragon);
    }

    @Shadow public abstract PhaseType<ChargingPlayerPhase> getType();

    @Shadow private int chargingTicks;

    @Inject(at = @At("HEAD"), method = "serverTick")
    public void serverTick(CallbackInfo ci) {
        if (DragonBattleManager.INSTANCE.isHomingDive()) {
            this.chargingTicks = 0;
            HomingSweepManager.INSTANCE.tick();
        }
    }

    @Inject(at = @At("TAIL"), method = "beginPhase")
    public void beginPhase(CallbackInfo ci) {
        if (DragonBattleManager.INSTANCE.isHomingDive()) {
            HomingSweepManager.INSTANCE.begin();
        }
    }

    @Override
    public void endPhase() {
        if (DragonBattleManager.INSTANCE.isHomingDive()) {
            HomingSweepManager.INSTANCE.end();
        }
    }
}
