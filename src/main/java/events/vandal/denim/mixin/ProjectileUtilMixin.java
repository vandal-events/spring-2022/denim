package events.vandal.denim.mixin;

import events.vandal.denim.Denim;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ProjectileUtil.class)
public class ProjectileUtilMixin {
    @Inject(at = @At("HEAD"), method = "createArrowProjectile")
    private static void getArrowCount(LivingEntity entity, ItemStack stack, float damageModifier, CallbackInfoReturnable<PersistentProjectileEntity> cir) {
        System.out.println(entity.getType());
        if (entity.getType() == EntityType.PLAYER) {
            var metrics = Denim.Companion.getMetrics().getMetric(((ServerPlayerEntity) entity).getGameProfile());

            metrics.setTotalArrowsShot(metrics.getTotalArrowsShot() + 1);
        }
    }
}
