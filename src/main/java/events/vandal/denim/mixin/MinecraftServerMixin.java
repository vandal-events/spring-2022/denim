package events.vandal.denim.mixin;

import events.vandal.denim.Denim;
import events.vandal.denim.boss.DragonBattleManager;
import events.vandal.denim.boss.fake.FakeDragonBattleManager;
import events.vandal.denim.enderoid.EnderoidManager;
import events.vandal.denim.server.DenimServer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftServer.class)
public class MinecraftServerMixin {
    @Inject(at = @At("HEAD"), method = "shutdown")
    public void shutdown(CallbackInfo ci) {
        FakeDragonBattleManager.INSTANCE.reset();
        DragonBattleManager.INSTANCE.reset();
        Denim.Companion.getMetrics().save();
        EnderoidManager.INSTANCE.save((ServerWorld) DenimServer.endWorld);
    }
}
