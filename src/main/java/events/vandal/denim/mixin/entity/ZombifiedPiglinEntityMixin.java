package events.vandal.denim.mixin.entity;

import events.vandal.denim.enderoid.EnderoidEntity;
import events.vandal.denim.enderoid.EnderoidEntityDuck;
import net.minecraft.entity.mob.ZombifiedPiglinEntity;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.LocalDifficulty;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ZombifiedPiglinEntity.class)
public class ZombifiedPiglinEntityMixin implements EnderoidEntityDuck {
    private boolean isEnderoid;
    private EnderoidEntity enderoidEntity;

    // A couple duck interface stuff. Very weird, I know.
    @Override
    public boolean isEnderoid() {
        return isEnderoid;
    }

    @Override
    public void setEnderoid(boolean isEnderoid) {
        this.isEnderoid = isEnderoid;
    }

    @NotNull
    @Override
    public EnderoidEntity getEnderoidEntity() {
        return enderoidEntity;
    }

    @Override
    public void setEnderoidEntity(@NotNull EnderoidEntity enderoidEntity) {
        this.enderoidEntity = enderoidEntity;
    }

    @Inject(at = @At("HEAD"), method = "initEquipment", cancellable = true)
    public void removeGoldenSword(Random random, LocalDifficulty localDifficulty, CallbackInfo ci) {
        if (this.isEnderoid)
            ci.cancel();
    }
}
