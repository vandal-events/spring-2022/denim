package events.vandal.denim.mixin.entity;

import com.mojang.authlib.GameProfile;
import events.vandal.denim.Denim;
import events.vandal.denim.enderoid.EnderoidManager;
import events.vandal.denim.metrics.DenimMetricData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin {
    @Shadow public abstract GameProfile getGameProfile();

    @Inject(at = @At("RETURN"), method = "damage")
    public void damage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        DenimMetricData metrics = Denim.Companion.getMetrics().getMetric(this.getGameProfile());
        metrics.setTotalDamageTaken(metrics.getTotalDamageTaken() + amount);

        if (((LivingEntity)(Object) this).getHealth() <= 0F) {
            metrics.setDeaths(metrics.getDeaths() + 1);
        } else {
            if (source.getAttacker() != null && source.getAttacker().getType() == EntityType.PLAYER &&
                    EnderoidManager.INSTANCE.getEnderoids().stream().anyMatch(entity -> entity.getMountedPlayerUUID() != null && entity.getMountedPlayerUUID().equals(((LivingEntity) (Object) this).getUuid()))
            ) {
                var enderoid = EnderoidManager.INSTANCE.getEnderoids().stream().filter(entity -> entity.getMountedPlayerUUID() != null && entity.getMountedPlayerUUID().equals(((LivingEntity) (Object) this).getUuid())).toList().get(0);
                enderoid.endPlayerMount(((LivingEntity) (Object) this).getPos());

                enderoid.getBaseEntity().damage(source, amount);

                var attackerMetrics = Denim.Companion.getMetrics().getMetric(((PlayerEntity) source.getAttacker()).getGameProfile());
                attackerMetrics.setEnderoidsKnocked(attackerMetrics.getEnderoidsKnocked() + 1);
            }
        }
    }

    @Inject(at = @At("HEAD"), method = "dropItem(Lnet/minecraft/item/ItemStack;ZZ)Lnet/minecraft/entity/ItemEntity;", cancellable = true)
    public void preventDroppingEnderoid(ItemStack stack, boolean throwRandomly, boolean retainOwnership, CallbackInfoReturnable<ItemEntity> cir) {
        if (stack == null)
            return;

        if (stack.getNbt() != null && stack.getNbt().containsUuid("enderoidUuid")) {
            var enderoidUuid = stack.getNbt().getUuid("enderoidUuid");
            if (EnderoidManager.INSTANCE.getEnderoids().stream().noneMatch(entity -> entity.getUuid().equals(enderoidUuid)))
                return;

            var enderoid = EnderoidManager.INSTANCE.getEnderoids().stream().filter(entity -> entity.getUuid().equals(enderoidUuid)).toList().get(0);
            enderoid.attemptDrop();

            cir.cancel();
        }
    }

    // how not to do arrows
    /*@Inject(at = @At("TAIL"), method = "getArrowType")
    public void getArrow(ItemStack stack, CallbackInfoReturnable<ItemStack> cir) {
        if (cir.getReturnValue() != ItemStack.EMPTY) {
            DenimMetricData metrics = Denim.Companion.getMetrics().getMetric(this.getGameProfile());
            metrics.setTotalArrowsShot(metrics.getTotalArrowsShot() + 1);
        }
    }*/
}
