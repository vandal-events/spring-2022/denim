package events.vandal.denim.mixin.item;

import events.vandal.denim.Denim;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BowItem.class)
public class BowItemMixin {
    @Inject(at = @At("TAIL"), method = "onStoppedUsing")
    public void incrementBowStat(ItemStack stack, World world, LivingEntity user, int remainingUseTicks, CallbackInfo ci) {
        if (user.getType() != EntityType.PLAYER)
            return;

        var metrics = Denim.Companion.getMetrics().getMetric(((PlayerEntity) user).getGameProfile());

        metrics.setTotalArrowsShot(metrics.getTotalArrowsShot() + 1);
    }
}
