package events.vandal.metrics

import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mojang.authlib.GameProfile
import net.fabricmc.loader.api.FabricLoader
import org.apache.logging.log4j.LogManager
import java.io.File
import java.lang.reflect.Constructor
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.companionObjectInstance
import kotlin.reflect.full.primaryConstructor

class MetricManager<T : MetricData>(private val dataClass: KClass<T>, val eventName: String) {
    private val logger = LogManager.getLogger(MetricManager::class.java)

    private val dirPath = File(FabricLoader.getInstance().configDir.toFile(), "vandal_metrics")
    private val metricsFilePath = File(dirPath, "metrics.json")

    private val metrics = mutableListOf<T>()
    private val saveTimer = Timer("$eventName MetricManager Save Timer")

    init {
        logger.info("Created metrics for Vandal Events event: $eventName")
        logger.info("Vandal Events: https://vandal.events / https://vandal.events/discord")

        val deserializeMethod = (dataClass.companionObjectInstance!!.javaClass).getMethod("deserialize", JsonObject::class.java)

        val rawData = getFromJson()
        rawData.forEach {
            if (!it.isJsonObject) {
                logger.debug("An element isn't a JsonObject, disposing.")
                return@forEach
            }

            try {
                val data = deserializeMethod.invoke(dataClass.companionObjectInstance, it.asJsonObject) as T
                metrics.add(data)
            } catch (e: Exception) {
                val uuid = UUID.randomUUID()

                logger.error("An element appears to be malformed, disposing. Malformed data stored under ${File(dirPath, "malformed_$uuid.json").absolutePath}.")
                File(dirPath, "malformed_$uuid.json").apply {
                    createNewFile()
                    writeText(it.asJsonObject.toString())
                }
            }
        }

        saveTimer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                this@MetricManager.save()
            }
        }, 0L, 30L * 60L * 1000L)
    }

    private fun getOrCreateFile(file: File): File {
        if (!dirPath.exists())
            dirPath.mkdir()

        if (!file.exists())
            file.parentFile.mkdirs()

        file.createNewFile()
        return file
    }

    fun getFromJson(): JsonArray {
        val file = getOrCreateFile(metricsFilePath)

        try {
            val json = if (file.readText().trim().isEmpty()) {
                JsonObject().apply {
                    add("metrics", JsonArray())
                    addProperty("eventName", eventName)
                }
            } else {
                JsonParser.parseString(file.readText()).asJsonObject
            }

            if (!json.has("metrics") || !json.get("metrics").isJsonArray) {
                logger.warn("Metrics file appears to be invalid, disposing of old metrics.")
                file.copyTo(File(dirPath, "broken_metrics.json"))
                file.delete()
                file.createNewFile()

                return JsonArray()
            }

            return json.getAsJsonArray("metrics")
        } catch (e: Exception) {
            logger.warn("Failed to parse metrics from file!", e)
            logger.warn("Disposing of old metrics.")
            file.copyTo(File(dirPath, "broken_metrics.json"))
            file.delete()
            file.createNewFile()
        }

        return JsonArray()
    }

    fun getMetric(data: GameProfile): T {
        return if (metrics.find { it.profile == data } != null)
            metrics.first { it.profile == data }
        else {
            val dataConstructor = dataClass.primaryConstructor
            val metrics = dataConstructor?.callBy(mapOf(dataConstructor.parameters.first() to data)) ?: throw Exception("Failed to call constructor")
            this.metrics.add(metrics)

            this.metrics.first { it.profile == data }
        }
    }

    fun save() {
        val jsonObj = JsonObject()
        val jsonArr = JsonArray()

        this.metrics.forEach {
            jsonArr.add(it.serialize())
        }

        jsonObj.add("metrics", jsonArr)
        jsonObj.addProperty("eventName", eventName)

        getOrCreateFile(metricsFilePath).writeText(GsonBuilder().apply {
            setPrettyPrinting()
        }.create().toJson(jsonObj))
    }
}