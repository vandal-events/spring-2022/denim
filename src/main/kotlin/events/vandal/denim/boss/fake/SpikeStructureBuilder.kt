package events.vandal.denim.boss.fake

import events.vandal.denim.mixin.structure.StructureAccessor
import events.vandal.denim.server.DenimServer
import net.minecraft.block.Block
import net.minecraft.block.Blocks
import net.minecraft.entity.Entity
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.entity.mob.ShulkerEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.structure.StructurePlacementData
import net.minecraft.structure.StructureTemplate
import net.minecraft.util.BlockMirror
import net.minecraft.util.BlockRotation
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Box
import net.minecraft.world.World
import net.minecraft.world.gen.feature.EndSpikeFeature

class SpikeStructureBuilder(val world: ServerWorld, val spike: EndSpikeFeature.Spike) {
    var hasNext = false
    val endCrystal: EndCrystalEntity
    var isComplete = false

    private var currentBuildHeight = 0
    private val structure: StructureTemplate?
    private val pos: BlockPos?

    private var height: Int = 0

    private val structureInfos: MutableList<StructureTemplate.StructureBlockInfo>?
    private val placementData = StructurePlacementData()
    private val processedInfo: MutableList<StructureTemplate.StructureBlockInfo>?

    init {
        val entities = world.getNonSpectatingEntities(EndCrystalEntity::class.java, spike.boundingBox)
        endCrystal = if (entities.isNotEmpty()) {
            entities[0]
        } else EndCrystalEntity(world, spike.centerX.toDouble() + .5, spike.height.toDouble() + 1, spike.centerZ.toDouble() + .5)

        if (entities.isEmpty()) {
            world.createExplosion(null, endCrystal.x, endCrystal.y, endCrystal.z, 3F, World.ExplosionSourceType.NONE)
            world.spawnEntity(endCrystal)
        }

        val determinedStructure = getStructureFromValues()

        if (determinedStructure != null) {
            structure = DenimServer.server.structureTemplateManager.getTemplateOrBlank(Identifier("denim", determinedStructure.replace("[EXT]", "") + "_${spike.height}" + if (determinedStructure.endsWith("[EXT]")) "_ext1" else ""))

            // Old code, don't think this even works tbh
            /*val basePos = Vec3i(spike.centerX - spike.radius, 49, spike.centerZ - spike.radius)
            //val box = Box(Vec3d.of(basePos), Vec3d((spike.centerX + spike.radius).toDouble(), spike.height.toDouble(), (spike.centerZ + spike.radius).toDouble()))

            // Should probably be called bottomPos, actually
            // This is done to search for the lowest point for the structures to spawn onto.
            var topPos = BlockPos(0, 0, 0)

            for (x in (spike.centerX - spike.radius)..(spike.centerX + spike.radius)) {
                for (z in (spike.centerZ - spike.radius)..(spike.centerZ + spike.radius)) {
                    val topPosCandidate = world.getTopPosition(Heightmap.Type.MOTION_BLOCKING, BlockPos(x, 0, z))

                    // I don't believe the island is allowed to be lower than 40 blocks, so yeah
                    if (topPosCandidate.y < 40)
                        continue

                    if (topPos.x == 0 && topPos.y == 0 && topPos.z == 0) {
                        topPos = topPosCandidate
                    }

                    if (topPosCandidate.y < topPos.y) {
                        topPos = topPosCandidate
                    }
                }
            }*/

            // This should try to determine where the structure goes by finding the position via the bedrock part below the End Crystal
            val nbt = structure!!.writeNbt(NbtCompound()) // WHY THE FUCK IS IT BACKWARDS

            val blocksList = nbt.getList("blocks", NbtList.COMPOUND_TYPE.toInt())
            val paletteList = nbt.getList("palette", NbtList.COMPOUND_TYPE.toInt())
            val bedrockId = paletteList.indexOf(NbtCompound().apply { // Kotlin my beloved
                putString("Name", "minecraft:bedrock")
            })

            var structureBedrockPos: BlockPos? = null

            blocksList.forEach {
                if (structureBedrockPos != null)
                    return@forEach

                val compound = it as NbtCompound

                if (compound.getInt("state") != bedrockId)
                    return@forEach

                val serializedPos = compound.getList("pos", NbtList.INT_TYPE.toInt())
                structureBedrockPos = BlockPos(serializedPos.getInt(0), serializedPos.getInt(1), serializedPos.getInt(2))
            }

            if (structureBedrockPos != null) {
                // This tries to find the position of the Bedrock in the structure,
                // and approximates where the structure will spawn for when the Bedrock is in the same position
                // in both the structure and spike.

                val spikeBedrockPos = BlockPos(spike.centerX, spike.height, spike.centerZ)
                var normalizedStructureBedrockPos = BlockPos(
                    (spike.centerX - spike.radius) + structureBedrockPos!!.x,
                    49, // just a random value
                    (spike.centerZ - spike.radius) + structureBedrockPos!!.z
                )

                normalizedStructureBedrockPos = normalizedStructureBedrockPos.add(
                    // X
                    if (spikeBedrockPos.x != normalizedStructureBedrockPos.x)
                        spikeBedrockPos.x - normalizedStructureBedrockPos.x
                    else
                        0,

                    // Y
                    if (spikeBedrockPos.y != normalizedStructureBedrockPos.y)
                        spikeBedrockPos.y - normalizedStructureBedrockPos.y
                    else
                        0,

                    // Z
                    if (spikeBedrockPos.z != normalizedStructureBedrockPos.z)
                        spikeBedrockPos.z - normalizedStructureBedrockPos.z
                    else
                        0,
                )

                pos = BlockPos(
                    normalizedStructureBedrockPos.x - structureBedrockPos!!.x,
                    normalizedStructureBedrockPos.y - structureBedrockPos!!.y,
                    normalizedStructureBedrockPos.z - structureBedrockPos!!.z
                )

                currentBuildHeight = pos.y
                height = structure.size.y

                structureInfos = placementData.getRandomBlockInfos((structure as StructureAccessor).blockInfoLists, pos).all.toMutableList()/*.apply {
                    if (determinedStructure.endsWith("[EXT]")) {
                        val newPos = pos.add(0, 48, 0)

                        val additionalStructure = DenimServer.server.structureManager.getStructureOrBlank(
                            Identifier(
                                "denim",
                                determinedStructure.replace("[EXT]", "") + "_${spike.height}" + "_ext2"
                            )
                        )

                        height += additionalStructure.size.y

                        this.addAll(placementData.getRandomBlockInfos(
                            (additionalStructure as StructureAccessor).blockInfoLists,
                            newPos
                        ).all.toMutableList())
                    }
                }*/
                processedInfo = StructureTemplate.process(world, pos, pos, placementData, structureInfos).apply {
                    if (determinedStructure.endsWith("[EXT]")) {
                        val newPos = pos.add(0, 48, 0)
                        val newPlacementData = StructurePlacementData()

                        val additionalStructure = DenimServer.server.structureTemplateManager.getTemplateOrBlank(Identifier("denim", determinedStructure.replace("[EXT]", "") + "_${spike.height}"  + "_ext2"))
                        val additionalStructureInfos = newPlacementData.getRandomBlockInfos((additionalStructure as StructureAccessor).blockInfoLists, newPos).all.toMutableList()

                        this.addAll(StructureTemplate.process(world, newPos, newPos, newPlacementData, additionalStructureInfos))
                    }
                }

                hasNext = true
            } else {
                pos = null
                structureInfos = null
                processedInfo = null

                isComplete = true
            }
        } else {
            structure = null
            pos = null
            structureInfos = null
            processedInfo = null

            isComplete = true
        }

        endCrystal.beamTarget = BlockPos(0, 156, 0)
    }

    fun animate(): Boolean {
        if (endCrystal.beamTarget?.y?.let { it <= 40 } == true) {
            return true
        }

        endCrystal.beamTarget = BlockPos(0, if (endCrystal.beamTarget == null) 156 else endCrystal.beamTarget!!.y - 1, 0)
        DenimServer.endWorld.playSound(null, endCrystal.beamTarget, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE, SoundCategory.HOSTILE, 0.6F, 0.8F)

        return false
    }

    private fun getStructureFromValues(): String? {
        return when (spike.height) {
            91 -> "parkour_tower"
            100 -> "parkour_tower[EXT]"
            82 -> "pearl_tower"
            103 -> "pearl_tower[EXT]"
            79, 94 -> "shulker_ext"
            76, 85 -> "shulker_int"
            88, 97 -> "simple_staircase"

            else -> null
        }
    }

    fun next() {
        if (!hasNext) {
            isComplete = true
            return
        }

        if (structure == null || pos == null) {
            isComplete = true
            hasNext = false
            return
        }

        val height = currentBuildHeight

        // Basically a very, very simplified and modified version of Structure#place()
        for (x in pos.x..(pos.x + structure.size.x)) {
            for (z in pos.z..(pos.z + structure.size.z)) {
                val currentPos = BlockPos(x, height, z)

                val structureBlockInfo = processedInfo!!.firstOrNull { it.pos == currentPos } ?: continue

                world.setBlockState(currentPos, structureBlockInfo.state, Block.NOTIFY_ALL, Block.SKIP_DROPS)
            }
        }

        hasNext = height < (pos.y + this.height)
        isComplete = !hasNext

        if (hasNext)
            currentBuildHeight++

        if (isComplete) {
            (this.structure as StructureAccessor).invokeSpawnEntities(world, pos, BlockMirror.NONE, BlockRotation.NONE, pos, null, true)
        }
    }

    fun reset() {
        if (pos == null || structure == null)
            return

        world.getNonSpectatingEntities(ShulkerEntity::class.java, spike.boundingBox).toMutableList().forEach {
            it.remove(Entity.RemovalReason.DISCARDED)
        }

        val box = Box(pos.toCenterPos(), pos.add(structure.size).toCenterPos())

        for (x in box.minX.toInt()..box.maxX.toInt()) {
            for (y in box.minY.toInt()..box.maxY.toInt()) {
                for (z in box.minZ.toInt()..box.maxZ.toInt()) {
                    val originalBlock = world.getBlockState(BlockPos(x, y, z))
                    if (listOf<Block>(
                            Blocks.END_STONE,
                    ).any { it == originalBlock.block })
                        continue

                    world.setBlockState(BlockPos(x, y, z), Blocks.AIR.defaultState, Block.NOTIFY_ALL)
                }
            }
        }

        currentBuildHeight = pos.y
        isComplete = false

        endCrystal.beamTarget = null
        endCrystal.remove(Entity.RemovalReason.DISCARDED)
    }
}