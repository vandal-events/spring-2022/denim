package events.vandal.denim.boss.phase

import events.vandal.denim.Denim
import events.vandal.denim.boss.DragonBattleManager
import events.vandal.denim.server.DenimServer
import net.minecraft.entity.Entity
import net.minecraft.entity.boss.dragon.EnderDragonEntity
import net.minecraft.entity.boss.dragon.phase.AbstractPhase
import net.minecraft.entity.boss.dragon.phase.Phase
import net.minecraft.entity.boss.dragon.phase.PhaseType
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.decoration.ArmorStandEntity
import net.minecraft.entity.decoration.EndCrystalEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.network.packet.s2c.play.EntityPositionS2CPacket
import net.minecraft.predicate.entity.EntityPredicates
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Box
import net.minecraft.util.math.Vec3d

class RisePhase(dragon: EnderDragonEntity) : AbstractPhase(dragon) {
    override fun clientTick() {
        // Guess what, we need none of this because everything needs to be handled on the server,
        // due to having to be completely constrained to being a server-side mod.
    }

    override fun serverTick() {
        // THIS FUCK WON'T REACH 100, WHAT THE FUCK
        if (dragon.pos.y >= 95 && !DragonBattleManager.loadBossBar) {
            dragon.isSilent = false
            dragon.isInvulnerable = true

            if (DragonBattleManager.dragon?.uuid != dragon.uuid) {
                println("The DragonBattleManager's dragon and the RisePhase's dragon aren't equal, running a reset.")
                DragonBattleManager.reset()
                return
            }

            DragonBattleManager.enableCinematic()
        }
    }

    override fun beginPhase() {}

    override fun endPhase() {}

    override fun getPathTarget(): Vec3d? {
        return Vec3d.ZERO.add(0.0, 100.0, 0.0)
    }

    override fun getMaxYAcceleration(): Float {
        return 15F
    }

    override fun getType(): PhaseType<out Phase> {
        return Denim.RISING_PHASE
    }
}