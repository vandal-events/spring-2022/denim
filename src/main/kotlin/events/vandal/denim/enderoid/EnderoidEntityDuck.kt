package events.vandal.denim.enderoid

// Duck interface for knowing if a zombified piglin is actually an Enderoid.
interface EnderoidEntityDuck {
    var isEnderoid: Boolean
    var enderoidEntity: EnderoidEntity
}